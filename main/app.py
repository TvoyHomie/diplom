import torch

from visualize import make_dataloaders
from visualize import visualize_pic

if __name__ == '__main__':
    model = torch.load("mymodel.pt")
    model.eval()
    picture = make_dataloaders(paths=["aboba.jpg"], split="val")
    pic_val = next(iter(picture))
    visualize_pic(model, pic_val, save=False)
    print('aboba')